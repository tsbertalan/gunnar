(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1444,         59]
NotebookOptionsPosition[      1143,         43]
NotebookOutlinePosition[      1479,         58]
CellTagsIndexPosition[      1436,         55]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{"y", "\[Equal]", 
    RowBox[{
     RowBox[{"32.6", " ", "x"}], " ", "+", " ", "25.49"}]}], ",", "x"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.6359766312757607`*^9, 3.635976644120466*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"x", "\[Rule]", 
    RowBox[{
     RowBox[{"-", "0.03067484662576687`"}], " ", 
     RowBox[{"(", 
      RowBox[{"25.49`", "\[VeryThinSpace]", "-", "y"}], ")"}]}]}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.6359766447562733`*^9}]
}, Open  ]]
},
WindowSize->{808, 911},
WindowMargins->{{1049, Automatic}, {46, Automatic}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 251, 7, 32, "Input"],
Cell[834, 31, 293, 9, 32, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

